#!/usr/bin/env sh

# show .env variables
# grep -v '^#' .env

# export .env variables
# export $(grep -v '^#' .env | xargs)

# export .env variables that has quotes and spaces into them
# Export env vars
set -o allexport
source .env
set +o allexport

echo ">>> .env variables export complete"

# Docs: https://stackoverflow.com/questions/19331497/set-environment-variables-from-file-of-key-value-pairs/20909045#20909045
# Website insipration: https://zwbetz.com/set-environment-variables-in-your-bash-shell-from-a-env-file/

# Usage:

# unset MY_VAR ## unset .env variable ##
# source set-env.sh ## set env variables in the shell  ##
# echo "${MY_VAR}, ${YOUR_VAR}" ## use .env variables ##
