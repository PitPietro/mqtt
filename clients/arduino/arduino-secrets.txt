#define WIFI_SSID "WIFI_NAME" // place your WiFi name inside the quotes
#define WIFI_PWD "WIFI_PASSWORD" // place your WiFi password inside the quotes
#define BROKER_IP "192.168.X.X" // place MQTT Broker IP address inside the quotes
#define BROKER_PORT XXXX // place MQTT Broker PORT without quotes (should be 1883 by default)
#define DEV_NAME  "CLIENT_ID" // place MQTT Client ID name inside the quotes
#define MQTT_USER "USERNAME" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
#define MQTT_PWD  "PASSWORD" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
