package com.pietropoluzzi.mqtt_samples.string_payload

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.pietropoluzzi.mqtt_samples.R
import com.pietropoluzzi.mqtt_samples.databinding.ActivityStringPublisherBinding
import com.pietropoluzzi.mqtt_samples.settings.EndpointSettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.SettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.UserSettingsActivity
import com.pietropoluzzi.mqtt_samples.utils.SimpleStringRecyclerView
import com.pietropoluzzi.mqtt_samples.utils.SimpleStringPayload
import info.mqtt.android.service.Ack
import org.eclipse.paho.client.mqttv3.*
import info.mqtt.android.service.MqttAndroidClient
import java.util.*

/**
 * TODO Display notification is some error occurs, like authentication or connection to MQTT broker
 */
class StringPublisherActivity : AppCompatActivity() {
    private val tag = this.javaClass.simpleName

    private lateinit var binding: ActivityStringPublisherBinding
    private lateinit var simpleStringRecyclerView: SimpleStringRecyclerView
    private lateinit var mqttClient: MqttAndroidClient
    private lateinit var recyclerView: RecyclerView
    // private lateinit var subSwitch: SwitchMaterial
    // private lateinit var topicEditText: TextView
    // private val lastTopic = ""
    // private var subSwitchState = false

    private lateinit var preferences: SharedPreferences
    private var ip: String? = null
    private var port: String? = null
    private var username: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_subscriber)

        // bind XML layout to the class
        binding = ActivityStringPublisherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // bind views to Activity's variables
        recyclerView = binding.payloadRecyclerView

        /* START payload configuration recycle view behaviour */
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = null

        // enable recycle view in reverse order: https://stackoverflow.com/a/63894078
        // val recyclerLayoutManager = LinearLayoutManager(this)
        // recyclerView.layoutManager = recyclerLayoutManager
        simpleStringRecyclerView =
            SimpleStringRecyclerView()
        recyclerView.adapter = simpleStringRecyclerView

        simpleStringRecyclerView.setOnAdapterItemClickListener {
            val childAdapterPosition = recyclerView.getChildAdapterPosition(it)
            val itemAtPosition = simpleStringRecyclerView.getItemAtPosition(childAdapterPosition)
            onAdapterItemClick(itemAtPosition)
        }
        /* END payload configuration recycle view behaviour */

        binding.publishBtn.setOnClickListener {
            val topic = binding.topicEditText.text.toString()
            val msg = binding.msgEditText.text.toString()

            publish(topic, msg)
        }
    }

    /**
     * Connect to MQTT broker with the username and password set in the related Settings activity
     *
     * @param username MQTT username
     * @param password MQTT password
     */
    private fun connectMQTT(username: String, password: String) {
        val options = MqttConnectOptions()
        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1;
        options.userName =  username //"mosquitto"
        options.password = password.toCharArray() // "mosquitto".toCharArray()
        options.isAutomaticReconnect = true
        options.isCleanSession = false

        try {
            mqttClient.connect(options, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Connection success: token = ${asyncActionToken.toString()}")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(
                        tag,
                        "Connection failure: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}"
                    )
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }

        mqttClient.setCallback(object : MqttCallbackExtended {
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                Log.d(tag, "messageArrived() TOPIC: $topic | msg = $message")

                val element = SimpleStringPayload(message.toString(), Date())
                simpleStringRecyclerView.addPayloadElement(element)
            }

            override fun connectionLost(cause: Throwable?) {
                Log.d(tag, "connectionLost() because of ${cause.toString()}")
            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {
                Log.d(tag, "deliveryComplete() token: ${token.toString()}")
            }

            override fun connectComplete(reconnect: Boolean, serverURI: String?) {
                Log.d(tag, "connectComplete() reconnect = $reconnect | URI = $serverURI")
            }
        })
    }

    /**
     * Perfmon action when clicking a recycle view's element
     */
    private fun onAdapterItemClick(element: SimpleStringPayload) {
        Log.d(tag, "onAdapterItemClick() element = $element")
    }

    /**
     * Publish a String message on a given topic to the MQTT broker
     *
     * @param topic Topic to which publish the message
     * @param msg Message to send
     * @param qos Maximum Quality of Service at which to subscribe
     */
    private fun publish(topic: String, msg: String, qos: Int = 1, retained: Boolean = false) {
        try {
            val message = MqttMessage()
            message.payload = msg.toByteArray()
            message.qos = qos
            message.isRetained = retained
            mqttClient.publish(topic, message, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "$msg published to $topic")


                    val element = SimpleStringPayload(msg, Date())
                    simpleStringRecyclerView.addPayloadElement(element)
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to publish $msg to $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    /**
     * Disconnect to MQTT broker
     */
    private fun disconnectMQTT() {
        try {
            mqttClient.disconnect(null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Disconnect success: token = ${asyncActionToken.toString()}")

                    // clear the list of payloads when disconnecting to the MQTT broker
                    simpleStringRecyclerView.clearElements()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to disconnect: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()

        disconnectMQTT()
    }

    /**
     * Settings related method
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Log.d(tag, "onCreateOptionsMenu()")

        // inflate the menu: adds items to the action bar if present
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    /**
     * Settings related method
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.action_settings) {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()

        // retrieve information from settings
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        ip = preferences.getString(EndpointSettingsActivity.IP_KEY, EndpointSettingsActivity.DEFAULT_IP_KEY)
        port = preferences.getString(EndpointSettingsActivity.PORT_KEY, EndpointSettingsActivity.DEFAULT_PORT_KEY)
        username = preferences.getString(UserSettingsActivity.USERNAME_KEY, UserSettingsActivity.DEFAULT_USERNAME_KEY)
        password = preferences.getString(UserSettingsActivity.PASSWORD_KEY, UserSettingsActivity.DEFAULT_PASSWORD_KEY)

        Log.d(tag, "user = $username  pwd = $password")
        // val host = "192.168.60.10" // put here your host IP address
        // val port = "1883" // default port = 1883

        // URI = [protocol]://[host-IP-address]:[host-port]
        val serverURI = "tcp://$ip:$port"
        val clientId = MqttClient.generateClientId()

        mqttClient = MqttAndroidClient(this.baseContext, serverURI, clientId,  Ack.AUTO_ACK)
        connectMQTT(username!!, password!!)
        /* END MQTT client configuration */
    }
}