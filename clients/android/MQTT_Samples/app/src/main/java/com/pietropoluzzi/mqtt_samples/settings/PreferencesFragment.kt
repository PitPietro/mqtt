package com.pietropoluzzi.mqtt_samples.settings

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.pietropoluzzi.mqtt_samples.R

class PreferencesFragment : PreferenceFragmentCompat() {

    /**
     *  Load the preferences from an XML resource
     */
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}