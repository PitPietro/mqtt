package com.pietropoluzzi.mqtt_samples.settings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pietropoluzzi.mqtt_samples.R

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preference)

        // replace the frame with the XML resource
        supportFragmentManager.beginTransaction().replace(R.id.activityPreference, PreferencesFragment()).commit()
    }
}