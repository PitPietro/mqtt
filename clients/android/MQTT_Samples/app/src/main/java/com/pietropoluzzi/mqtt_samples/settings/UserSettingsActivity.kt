package com.pietropoluzzi.mqtt_samples.settings

import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.pietropoluzzi.mqtt_samples.R
import com.pietropoluzzi.mqtt_samples.databinding.ActivityUserSettingsBinding


class UserSettingsActivity : AppCompatActivity() {
    private lateinit var preferences: SharedPreferences
    // private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityUserSettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUserSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // set up the action bar
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.back_arrow_black_24)
        }
        supportActionBar!!.title = "User"

        // Get the shared preferences that work in concert with Android’' overall preference framework
        preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        // if IP or PORT has already been saved, populate the relative view
        if(preferences.contains(USERNAME_KEY)) {
            binding.username.setText(preferences.getString(USERNAME_KEY, DEFAULT_USERNAME_KEY))
        }
        if(preferences.contains(PASSWORD_KEY)) {
            binding.password.setText(preferences.getString(PASSWORD_KEY, DEFAULT_PASSWORD_KEY))
        }

        // docs: https://stackoverflow.com/questions/9307680/show-the-password-with-edittext
        binding.showPasswordSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (!isChecked) {
                // set to 129
                binding.password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

                /* Perform a bitwise OR on them:
                00000001
                10000000
                --------
                10000001 which is 129 */
            } else {
                binding.password.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
        }

        binding.saveUserInformation.setOnClickListener(this::onSaveBtnClick)
    }

    private fun onSaveBtnClick(view: View) {
        val usernameText = binding.username.text.toString()
        val passwordText = binding.password.text.toString()

        val editor: SharedPreferences.Editor = preferences.edit()

        // initialize an array list with the length equal to the settings to save
        val toSave = ArrayList<String>(2)

        toSave.add(usernameText)
        toSave.add(passwordText)

        if(toSave.contains("")) {
            Toast.makeText(this, R.string.add_all_info, Toast.LENGTH_LONG).show()
        } else {
            editor.putString(USERNAME_KEY, usernameText)
            editor.putString(PASSWORD_KEY, passwordText)
            editor.apply()
        }

        // back to parent activity
        // https://stackoverflow.com/questions/72634225/onbackpressed-deprecated-what-is-the-alternative
        onBackPressedDispatcher.onBackPressed()

        // move to a specific Activity
        // val myIntent = Intent(this@UserSettingsActivity, MainActivity::class.java)
        // this@UserSettingsActivity.startActivity(myIntent)
    }


    companion object {
        const val USERNAME_KEY = "username_key"
        const val PASSWORD_KEY = "password_key"

        const val DEFAULT_USERNAME_KEY = "mosquitto"
        const val DEFAULT_PASSWORD_KEY = "mosquitto"
    }
}