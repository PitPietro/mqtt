package com.pietropoluzzi.mqtt_samples.utils

import kotlinx.serialization.Serializable

@Serializable
class JsonPayload(val integer: Int, val double: Double, val string: String)