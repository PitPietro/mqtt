package com.pietropoluzzi.mqtt_samples.string_payload

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.switchmaterial.SwitchMaterial
import com.pietropoluzzi.mqtt_samples.R
import com.pietropoluzzi.mqtt_samples.databinding.ActivityStringSubscriberBinding
import com.pietropoluzzi.mqtt_samples.settings.EndpointSettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.SettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.UserSettingsActivity
import com.pietropoluzzi.mqtt_samples.utils.SimpleStringRecyclerView
import com.pietropoluzzi.mqtt_samples.utils.SimpleStringPayload
import info.mqtt.android.service.Ack
//import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import info.mqtt.android.service.MqttAndroidClient
import java.util.*

/**
 * Docs: [emqx.com](https://www.emqx.com/en/blog/android-connects-mqtt-using-kotlin)
 *
 * Eclipse Paho Android doesn't work with Android 12 and above, according to this stackoverflow
 * [answer](https://stackoverflow.com/questions/71155187/android-paho-mqtt-crashes-android-12-targeting-s-version-31-and-above-requi)
 *
 * Do not allow to edit the topic text view while the switch is ON
 * Turn the switch off when moving to a different activity to prevent MQTT authentication issues
 *
 * TODO Display notification is some error occurs, like authentication or connection to MQTT broker
 */
class StringSubscriberActivity : AppCompatActivity() {
    private val tag = this.javaClass.simpleName

    private lateinit var binding: ActivityStringSubscriberBinding
    private lateinit var simpleStringRecyclerView: SimpleStringRecyclerView
    private lateinit var mqttClient: MqttAndroidClient
    private lateinit var recyclerView: RecyclerView
    private lateinit var subSwitch: SwitchMaterial
    private lateinit var topicEditText: TextView
    // private val lastTopic = ""
    private var subSwitchState = false

    private lateinit var preferences: SharedPreferences
    private var ip: String? = null
    private var port: String? = null
    private var username: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_subscriber)

        // bind XML layout to the class
        binding = ActivityStringSubscriberBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // bind views to Activity's variables
        topicEditText = binding.topicEditText
        recyclerView = binding.payloadRecyclerView

        /* START payload configuration recycle view behaviour */
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = null

        // enable recycle view in reverse order: https://stackoverflow.com/a/63894078
        // val recyclerLayoutManager = LinearLayoutManager(this)
        // recyclerView.layoutManager = recyclerLayoutManager
        simpleStringRecyclerView =
            SimpleStringRecyclerView()
        recyclerView.adapter = simpleStringRecyclerView

        simpleStringRecyclerView.setOnAdapterItemClickListener {
            val childAdapterPosition = recyclerView.getChildAdapterPosition(it)
            val itemAtPosition = simpleStringRecyclerView.getItemAtPosition(childAdapterPosition)
            onAdapterItemClick(itemAtPosition)
        }
        /* END payload configuration recycle view behaviour */
    }

    /**
     * Connect to MQTT broker with the username and password set in the related Settings activity
     *
     * @param username MQTT username
     * @param password MQTT password
     */
    private fun connectMQTT(username: String, password: String) {
        val options = MqttConnectOptions()
        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1;
        options.userName =  username //"mosquitto"
        options.password = password.toCharArray() // "mosquitto".toCharArray()
        options.isAutomaticReconnect = true
        options.isCleanSession = false

        try {
            mqttClient.connect(options, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Connection success: token = ${asyncActionToken.toString()}")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(
                        tag,
                        "Connection failure: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}"
                    )
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }

        mqttClient.setCallback(object : MqttCallbackExtended {

            /**
             * This method is called when a message arrives from the server.
             * @param topic name of the topic on the message was published to.
             * @param message the actual message.
             * @throws Exception if a terminal error has occurred, and the client should be shut down.
             */
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                Log.d(tag, "messageArrived() TOPIC: $topic | msg = $message")

                val element = SimpleStringPayload(message.toString(), Date())
                simpleStringRecyclerView.addPayloadElement(element)
            }

            /**
             * This method is called when the connection to the server is lost.
             * @param cause the reason behind the loss of connection.
             */
            override fun connectionLost(cause: Throwable?) {
                Log.d(tag, "connectionLost() because of ${cause.toString()}")
            }

            /**
             * Called when delivery for a message has been completed, and all acknowledgments have been received.
             * For QoS `0` messages it's called once the message has been handed to the network for delivery.
             * For QoS `1` it is called when `PUBACK` is received and for QoS `2` when `PUBCOMP` is received.
             * The token will be the same token as that returned when the message was published.
             *
             * @param token the delivery token associated with the message.
             */
            override fun deliveryComplete(token: IMqttDeliveryToken?) {
                Log.d(tag, "deliveryComplete() token: ${token.toString()}")
            }

            /**
             * Called when the connection to the server is completed successfully.
             * @param reconnect If true, the connection was the result of automatic reconnect.
             * @param serverURI The server URI that the connection was made to.
             */
            override fun connectComplete(reconnect: Boolean, serverURI: String?) {
                Log.d(tag, "connectComplete() reconnect = $reconnect | URI = $serverURI")
            }
        })
    }

    /**
     * Perfmon action when clicking a recycle view's element
     */
    private fun onAdapterItemClick(element: SimpleStringPayload) {
        Log.d(tag, "onAdapterItemClick() element = $element")
    }

    /**
     * Subscribe to the given topic whit the given QoS value
     *
     * @param topic Topic to which subscribe
     * @param qos Maximum Quality of Service at which to subscribe
     */
    private fun subscribe(topic: String, qos: Int = 1) {
        try {
            mqttClient.subscribe(topic, qos, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Subscribed to topic $topic")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to subscribe to topic $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    /**
     * Unsubscribe to the topic given as parameter
     *
     * @param topic Topic to which unsubscribe
     */
    private fun unsubscribe(topic: String) {
        try {
            mqttClient.unsubscribe(topic, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Unsubscribed to topic $topic")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to unsubscribe to topic $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    /**
     * Disconnect to MQTT broker
     */
    private fun disconnectMQTT() {
        try {
            mqttClient.disconnect(null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Disconnect success: token = ${asyncActionToken.toString()}")

                    // clear the list of payloads when disconnecting to the MQTT broker
                    simpleStringRecyclerView.clearElements()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to disconnect: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()

        subSwitch.isChecked = false
        topicEditText.isEnabled = true
        unsubscribe(topicEditText.text.toString())

        disconnectMQTT()
    }

    /**
     * Settings related method
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Log.d(tag, "onCreateOptionsMenu()")

        // inflate the menu: adds items to the action bar if present
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    /**
     * Settings related method
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.action_settings) {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()

        // retrieve information from settings
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        ip = preferences.getString(EndpointSettingsActivity.IP_KEY, EndpointSettingsActivity.DEFAULT_IP_KEY)
        port = preferences.getString(EndpointSettingsActivity.PORT_KEY, EndpointSettingsActivity.DEFAULT_PORT_KEY)
        username = preferences.getString(UserSettingsActivity.USERNAME_KEY, UserSettingsActivity.DEFAULT_USERNAME_KEY)
        password = preferences.getString(UserSettingsActivity.PASSWORD_KEY, UserSettingsActivity.DEFAULT_PASSWORD_KEY)

        Log.d(tag, "user = $username  pwd = $password")

        // URI = [protocol]://[host-IP-address]:[host-port]
        val serverURI = "tcp://$ip:$port"
        val clientId = MqttClient.generateClientId()

        mqttClient = MqttAndroidClient(this.baseContext, serverURI, clientId,  Ack.AUTO_ACK)
        connectMQTT(username!!, password!!)
        /* END MQTT client configuration */

        // bind the switch variable to the layout's button
        // add a listener and save the switch state
        subSwitch = binding.subSwitch
        subSwitch.setOnCheckedChangeListener{_, isChecked ->
            subSwitchState = isChecked

            if(isChecked) {
                topicEditText.isEnabled = false

                // subscribe to topic
                subscribe(topicEditText.text.toString(), 1)
            } else {
                // unsubscribe to topic
                unsubscribe(topicEditText.text.toString())

                topicEditText.isEnabled = true
            }
        }
    }
}