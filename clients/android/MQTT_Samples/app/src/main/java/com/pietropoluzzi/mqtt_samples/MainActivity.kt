package com.pietropoluzzi.mqtt_samples

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import com.pietropoluzzi.mqtt_samples.databinding.ActivityMainBinding
import com.pietropoluzzi.mqtt_samples.json_payload.JsonPublisherActivity
import com.pietropoluzzi.mqtt_samples.json_payload.JsonSubscriberActivity
import com.pietropoluzzi.mqtt_samples.settings.SettingsActivity
import com.pietropoluzzi.mqtt_samples.string_payload.StringPublisherActivity
import com.pietropoluzzi.mqtt_samples.string_payload.StringSubscriberActivity

/**
 * ## Put variable inside string resources
 *
 * [docs](https://stackoverflow.com/questions/5854647/how-to-put-variable-inside-string-resources)
 *
 * Inside a Kotlin Activity or Fragment:
 * ```kotlin
 * myTextView.text = resources.getString(R.string.string_resource)
 *
 * // string resource with values
 * val floatValue: Float = 2.4F
 * myTextView.text = resources.getString(R.string.float_string, floatValue)
 * ```
 *
 * With a `string.xml` file:
 * ```xml
 * <string name="string_resource">I am a string</string>
 * <string name="float_string">My float value: %1$.2f °C</string>
 * ```
 */
class MainActivity : AppCompatActivity() {
    private val tag = this.javaClass.simpleName

    // private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    // private lateinit var topAppBar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.simpleSubBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, StringSubscriberActivity::class.java)
            startActivity(intent)
        }

        binding.simplePubBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, StringPublisherActivity::class.java)
            startActivity(intent)
        }

        binding.jsonSubBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, JsonSubscriberActivity::class.java)
            startActivity(intent)
        }

        binding.jsonPubBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, JsonPublisherActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Log.d(tag, "onCreateOptionsMenu()")

        // inflate the menu: adds items to the action bar if present
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.action_settings) {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}