package com.pietropoluzzi.mqtt_samples.json_payload

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.switchmaterial.SwitchMaterial
import com.pietropoluzzi.mqtt_samples.R
import com.pietropoluzzi.mqtt_samples.databinding.ActivityJsonPublisherBinding
import com.pietropoluzzi.mqtt_samples.databinding.ActivityJsonSubscriberBinding
import com.pietropoluzzi.mqtt_samples.settings.EndpointSettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.SettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.UserSettingsActivity
import com.pietropoluzzi.mqtt_samples.utils.JsonPayload
import com.pietropoluzzi.mqtt_samples.utils.SimpleJsonRecyclerView
import info.mqtt.android.service.Ack
import info.mqtt.android.service.MqttAndroidClient
import kotlinx.serialization.json.*
import org.eclipse.paho.client.mqttv3.*
import org.json.JSONObject
import java.util.*


/**
 * ## JSON fields types
 * According to [jsonschema2pojo.org](https://www.jsonschema2pojo.org/)
 *
 * | Schema type | Java type            | Kotlin type              |
 * |:-----------:|:--------------------:|:------------------------:|
 * | `string`    | `java.lang.String`  | `kotlin.String`          |
 * | `number`    | `java.lang.Double`  | `kotlin.Double`          |
 * | `integer`   | `java.lang.Integer` | `kotlin.Int`             |
 * | `object`    | _generated object_   | _generated object_       |
 * | `array`     | `java.util.List`    | `kotlin.Array`           |
 * | `array`*    | `java.util.Set`     | `kotlin.collections.Set` |
 * | `null`      | `java.lang.Object`  | _anonymous objects_      |
 * | `any`       | `java.lang.Object`  | `kotlin.Any`             |
 *
 * <b>*</b> without duplicates
 *
 * <hr>
 *
 * ## Kotlin reference
 * - [`kotlin.String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/)
 * - [`kotlin.Double`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/)
 * - [`kotlin.Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/)
 * - [`kotlin.Array`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/)
 * - [`kotlin.collections.Set`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-set/)
 * - [anonymous objects](https://kotlinlang.org/docs/object-declarations.html)
 *  [`kotlin.Any`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/)
 *
 *  To test this activity open a terminal and type:
 *  ```shell
 *  ➜  ~ mosquitto_pub -t Hello -m "{\"integer\":123,\"double\":3.14,\"string\":\"ABC\"}" -u "USERNAME" -P "PASSWORD"
 *  ➜  ~ mosquitto_pub -t Hello -m "{"integer":123,"double":3.14,"string":"ABC"}" -u "USERNAME" -P "PASSWORD"
 *  ```
 * Be sure to follow my blog post about [MQTT Protocol for IoT](https://pietropoluzzi.it/blog/iot/mqtt-protocol/).
 *
 */
class JsonSubscriberActivity : AppCompatActivity() {
    private val tag = this.javaClass.simpleName

    private lateinit var binding: ActivityJsonSubscriberBinding
    private lateinit var simpleJsonRecyclerView: SimpleJsonRecyclerView
    private lateinit var mqttClient: MqttAndroidClient
    private lateinit var recyclerView: RecyclerView
    private lateinit var subSwitch: SwitchMaterial
    private lateinit var topicEditText: TextView
    private var subSwitchState = false

    private lateinit var preferences: SharedPreferences
    private var ip: String? = null
    private var port: String? = null
    private var username: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_subscriber)

        // bind XML layout to the class
        binding = ActivityJsonSubscriberBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // bind views to Activity's variables
        topicEditText = binding.topicEditText
        recyclerView = binding.payloadRecyclerView
        // bind views to Activity's variables

        /* START payload configuration recycle view behaviour */
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = null

        // enable recycle view in reverse order: https://stackoverflow.com/a/63894078
        // val recyclerLayoutManager = LinearLayoutManager(this)
        // recyclerView.layoutManager = recyclerLayoutManager
        simpleJsonRecyclerView =
            SimpleJsonRecyclerView()
        recyclerView.adapter = simpleJsonRecyclerView

        simpleJsonRecyclerView.setOnAdapterItemClickListener {
            val childAdapterPosition = recyclerView.getChildAdapterPosition(it)
            val itemAtPosition = simpleJsonRecyclerView.getItemAtPosition(childAdapterPosition)
            onAdapterItemClick(itemAtPosition)
        }
        /* END payload configuration recycle view behaviour */
    }

    /**
     * Connect to MQTT broker with the username and password set in the related Settings activity
     *
     * @param username MQTT username
     * @param password MQTT password
     */
    private fun connectMQTT(username: String, password: String) {
        val options = MqttConnectOptions()
        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1
        options.userName = username //"mosquitto"
        options.password = password.toCharArray() // "mosquitto".toCharArray()
        options.isAutomaticReconnect = true
        options.isCleanSession = false

        try {
            mqttClient.connect(options, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Connection success: token = ${asyncActionToken.toString()}")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(
                        tag,
                        "Connection failure: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}"
                    )
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }

        mqttClient.setCallback(object : MqttCallbackExtended {

            /**
             * This method is called when a message arrives from the server.
             *
             * When the message arrives, it decodes the JSON elements.
             * @param topic name of the topic on the message was published to.
             * @param message the actual message.
             * @throws Exception if a terminal error has occurred, and the client should be shut down.
             */
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                Log.d(tag, "messageArrived() TOPIC: $topic | msg = $message")

                // convert String to JSON in order to extract the fields
                val json = JSONObject(message.toString())

                // test decoded JSON field
                // Log.d(tag, "TEST: int ${json.optInt("integer")} --- double ${json.optDouble("double")} --- string ${json.optString("string")}")

                val payload = JsonPayload(json.optInt("integer"), json.optDouble("double"), json.optString("string"))
                val element = Pair<JsonPayload, Date>(payload, Date())
                simpleJsonRecyclerView.addPayloadElement(element)
            }

            override fun connectionLost(cause: Throwable?) {
                Log.d(tag, "connectionLost() because of ${cause.toString()}")
            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {
                Log.d(tag, "deliveryComplete() token: ${token.toString()}")
            }

            override fun connectComplete(reconnect: Boolean, serverURI: String?) {
                Log.d(tag, "connectComplete() reconnect = $reconnect | URI = $serverURI")
            }
        })
    }

    /**
     * Perfmon action when clicking a recycle view's element
     */
    private fun onAdapterItemClick(element: Pair<JsonPayload, Date>) {
        Log.d(tag, "onAdapterItemClick() element = ${Json.encodeToJsonElement(JsonPayload.serializer(), element.first)}")
    }

    /**
     * Subscribe to the given topic whit the given QoS value
     *
     * @param topic Topic to which subscribe
     * @param qos Maximum Quality of Service at which to subscribe
     */
    private fun subscribe(topic: String, qos: Int = 1) {
        try {
            mqttClient.subscribe(topic, qos, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Subscribed to topic $topic")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to subscribe to topic $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    /**
     * Unsubscribe to the topic given as parameter
     *
     * @param topic Topic to which unsubscribe
     */
    private fun unsubscribe(topic: String) {
        try {
            mqttClient.unsubscribe(topic, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Unsubscribed to topic $topic")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to unsubscribe to topic $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    /**
     * Disconnect to MQTT broker
     */
    private fun disconnectMQTT() {
        try {
            mqttClient.disconnect(null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Disconnect success: token = ${asyncActionToken.toString()}")

                    // clear the list of payloads when disconnecting to the MQTT broker
                    simpleJsonRecyclerView.clearElements()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(
                        tag,
                        "Failed to disconnect: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}"
                    )
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()

        subSwitch.isChecked = false
        topicEditText.isEnabled = true
        unsubscribe(topicEditText.text.toString())


        disconnectMQTT()
    }

    /**
     * Settings related method
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Log.d(tag, "onCreateOptionsMenu()")

        // inflate the menu: adds items to the action bar if present
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    /**
     * Settings related method
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.action_settings) {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()

        // retrieve information from settings
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        ip = preferences.getString(
            EndpointSettingsActivity.IP_KEY,
            EndpointSettingsActivity.DEFAULT_IP_KEY
        )
        port = preferences.getString(
            EndpointSettingsActivity.PORT_KEY,
            EndpointSettingsActivity.DEFAULT_PORT_KEY
        )
        username = preferences.getString(
            UserSettingsActivity.USERNAME_KEY,
            UserSettingsActivity.DEFAULT_USERNAME_KEY
        )
        password = preferences.getString(
            UserSettingsActivity.PASSWORD_KEY,
            UserSettingsActivity.DEFAULT_PASSWORD_KEY
        )

        Log.d(tag, "user = $username  pwd = $password")
        // val host = "192.168.60.10" // put here your host IP address
        // val port = "1883" // default port = 1883

        // URI = [protocol]://[host-IP-address]:[host-port]
        val serverURI = "tcp://$ip:$port"
        val clientId = MqttClient.generateClientId()

        mqttClient = MqttAndroidClient(this.baseContext, serverURI, clientId, Ack.AUTO_ACK)
        connectMQTT(username!!, password!!)
        /* END MQTT client configuration */

        // bind the switch variable to the layout's button
        // add a listener and save the switch state
        subSwitch = binding.subSwitch
        subSwitch.setOnCheckedChangeListener{_, isChecked ->
            subSwitchState = isChecked

            if(isChecked) {
                topicEditText.isEnabled = false

                // subscribe to topic
                subscribe(topicEditText.text.toString(), 1)
            } else {
                // unsubscribe to topic
                unsubscribe(topicEditText.text.toString())

                topicEditText.isEnabled = true
            }
        }
    }
}