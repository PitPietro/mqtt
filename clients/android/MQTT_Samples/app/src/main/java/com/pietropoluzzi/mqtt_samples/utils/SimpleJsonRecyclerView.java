package com.pietropoluzzi.mqtt_samples.utils;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pietropoluzzi.mqtt_samples.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kotlin.Pair;

/**
 * TODO: Translate to Kotlin ASAP
 */
public class SimpleJsonRecyclerView extends RecyclerView.Adapter<SimpleJsonRecyclerView.JsonViewHolder> {
    private static final Comparator<Pair<JsonPayload, Date>> SORTING_COMPARATOR = Comparator.comparing(Pair::component2);
    private final String tag = "SimpleJsonRecyclerView";
    private final List<Pair<JsonPayload, Date>> data = new ArrayList<>();
    public OnAdapterItemClickListener onAdapterItemClickListener;
    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onAdapterItemClickListener != null) {
                onAdapterItemClickListener.onAdapterViewClick(v);
            }
        }
    };

    /**
     * Receive a Pair of `<JsonPayload, Date>` in order to avoid having to put the `Date` on the sent data
     * @param element
     */
    public void addPayloadElement(Pair<JsonPayload, Date> element) {
        Log.d(tag, "addPayloadElement()");

        // Pair<JsonPayload, Date> element = new Pair<>(payload, date);

        data.add(element);
        data.sort(SORTING_COMPARATOR);
        // notifyItemChanged(data.size() - 1);
        notifyDataSetChanged();
    }

    public void clearElements() {
        int elementsNumber = data.size() - 1;
        data.clear();

        // notifyItemRangeRemoved(0, elementsNumber);
        notifyDataSetChanged();
    }

    public Pair<JsonPayload, Date> getItemAtPosition(int childAdapterPosition) {
        Log.d(tag, "getItemAtPosition()");

        return data.get(childAdapterPosition);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @NonNull
    @Override
    public JsonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_json_payload_element, parent, false);
        itemView.setOnClickListener(onClickListener);
        return new JsonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull JsonViewHolder holder, int position) {
        Pair<JsonPayload, Date> item = data.get(position);
        // final ScanResult rxBleScanResult = data.get(position);

        holder.intTextView.setText(String.valueOf(item.getFirst().getInteger()));
        holder.doubleTextView.setText(String.valueOf(item.getFirst().getDouble()));
        holder.stringTextView.setText(item.getFirst().getString());

        Date date = item.getSecond();

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String stringDate = dateFormatter.format(date);

        dateFormatter.applyPattern("HH:mm:ss");
        String stringTime = dateFormatter.format(date);

        holder.dateTextView.setText(stringDate);
        holder.timeTextView.setText(stringTime);
    }

    public void setOnAdapterItemClickListener(OnAdapterItemClickListener onAdapterItemClickListener) {
        this.onAdapterItemClickListener = onAdapterItemClickListener;
    }

    public interface OnAdapterItemClickListener {
        void onAdapterViewClick(View view);
    }

    /**
     * TODO write a better recycle view layout component
     */
    static class JsonViewHolder extends RecyclerView.ViewHolder {
        TextView intTextView;
        TextView doubleTextView;
        TextView stringTextView;
        TextView timeTextView;
        TextView dateTextView;

        JsonViewHolder(View itemView) {
            super(itemView);
            intTextView = itemView.findViewById(R.id.intValue);
            doubleTextView = itemView.findViewById(R.id.doubleValue);
            stringTextView = itemView.findViewById(R.id.stringValue);
            timeTextView = itemView.findViewById(R.id.timeTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
        }
    }
}
