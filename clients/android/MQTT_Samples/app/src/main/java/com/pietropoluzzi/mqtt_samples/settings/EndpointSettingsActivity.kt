package com.pietropoluzzi.mqtt_samples.settings

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import com.pietropoluzzi.mqtt_samples.R
import com.pietropoluzzi.mqtt_samples.databinding.ActivityEndpointSettingsBinding

class EndpointSettingsActivity : AppCompatActivity() {
    private lateinit var preferences: SharedPreferences

    // private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityEndpointSettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityEndpointSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // set up the action bar
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.back_arrow_black_24)
        }
        supportActionBar!!.title = resources.getString(R.string.endpoint) // "Endpoint"

        // Get the shared preferences that work in concert with Android’' overall preference framework
        preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        // if IP or PORT has already been saved, populate the relative view
        if (preferences.contains(IP_KEY)) {
            binding.ip.setText(preferences.getString(IP_KEY, DEFAULT_IP_KEY))
        }
        if (preferences.contains(PORT_KEY)) {
            binding.port.setText(preferences.getString(PORT_KEY, DEFAULT_PORT_KEY))
        }

        binding.saveUserInformation.setOnClickListener(this::onSaveBtnClick)
    }

    private fun onSaveBtnClick(view: View) {
        val ipTextString = binding.ip.text.toString()
        val portTextString = binding.port.text.toString()

        val editor: SharedPreferences.Editor = preferences.edit()

        // initialize an array list with the length equal to the settings to save
        val toSave = ArrayList<String>(2)

        toSave.add(ipTextString)
        toSave.add(portTextString)

        if (toSave.contains("")) {
            Toast.makeText(this, R.string.add_all_info, Toast.LENGTH_LONG).show()
        } else {
            editor.putString(IP_KEY, ipTextString)
            editor.putString(PORT_KEY, portTextString)
            editor.apply()
        }

        // back to parent activity
        // https://stackoverflow.com/questions/72634225/onbackpressed-deprecated-what-is-the-alternative
        onBackPressedDispatcher.onBackPressed()

        // move to a specific Activity
        // val myIntent = Intent(this@EndpointSettingsActivity, MainActivity::class.java)
        // this@EndpointSettingsActivity.startActivity(myIntent)
    }


    companion object {
        const val IP_KEY = "ip_key"
        const val PORT_KEY = "port_key"

        const val DEFAULT_IP_KEY = "http://192.168.0.0"
        const val DEFAULT_PORT_KEY = "1883"
    }
}