package com.pietropoluzzi.mqtt_samples.utils;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pietropoluzzi.mqtt_samples.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * TODO: Translate to Kotlin ASAP
 */
public class SimpleStringRecyclerView extends RecyclerView.Adapter<SimpleStringRecyclerView.ViewHolder> {
    private static final Comparator<SimpleStringPayload> SORTING_COMPARATOR = Comparator.comparing(SimpleStringPayload::getDate);
    private final String tag = "SimpleStringRecyclerView";
    private final List<SimpleStringPayload> data = new ArrayList<>();
    public OnAdapterItemClickListener onAdapterItemClickListener;
    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (onAdapterItemClickListener != null) {
                onAdapterItemClickListener.onAdapterViewClick(v);
            }
        }
    };

    public void addPayloadElement(SimpleStringPayload element) {
        Log.d(tag, "addPayloadElement()");

        data.add(element);
        data.sort(SORTING_COMPARATOR);
        // notifyItemChanged(data.size() - 1);
        notifyDataSetChanged();
    }

    public void clearElements() {
        int elementsNumber = data.size() - 1;
        data.clear();

        // notifyItemRangeRemoved(0, elementsNumber);
        notifyDataSetChanged();
    }

    public SimpleStringPayload getItemAtPosition(int childAdapterPosition) {
        Log.d(tag, "getItemAtPosition()");

        return data.get(childAdapterPosition);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SimpleStringPayload item = data.get(position);
        // final ScanResult rxBleScanResult = data.get(position);
        holder.payloadTextView.setText(item.getPayload());

        Date date = item.getDate();

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String stringDate = dateFormatter.format(date);

        dateFormatter.applyPattern("HH:mm:ss");
        String stringTime = dateFormatter.format(date);

        holder.dateTextView.setText(stringDate);
        holder.timeTextView.setText(stringTime);
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_string_payload_element, parent, false);
        itemView.setOnClickListener(onClickListener);
        return new ViewHolder(itemView);
    }

    public void setOnAdapterItemClickListener(OnAdapterItemClickListener onAdapterItemClickListener) {
        this.onAdapterItemClickListener = onAdapterItemClickListener;
    }

    public interface OnAdapterItemClickListener {
        void onAdapterViewClick(View view);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView payloadTextView;
        TextView timeTextView;
        TextView dateTextView;

        ViewHolder(View itemView) {
            super(itemView);
            payloadTextView = itemView.findViewById(R.id.payloadTextView);
            timeTextView = itemView.findViewById(R.id.timeTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);

        }
    }
}
