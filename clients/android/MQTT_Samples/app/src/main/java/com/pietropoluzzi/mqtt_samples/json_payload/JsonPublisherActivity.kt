package com.pietropoluzzi.mqtt_samples.json_payload

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.pietropoluzzi.mqtt_samples.R
import com.pietropoluzzi.mqtt_samples.databinding.ActivityJsonPublisherBinding
import com.pietropoluzzi.mqtt_samples.settings.EndpointSettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.SettingsActivity
import com.pietropoluzzi.mqtt_samples.settings.UserSettingsActivity
import com.pietropoluzzi.mqtt_samples.utils.JsonPayload
import com.pietropoluzzi.mqtt_samples.utils.SimpleJsonRecyclerView
import info.mqtt.android.service.Ack
import info.mqtt.android.service.MqttAndroidClient
import kotlinx.serialization.json.Json
import org.eclipse.paho.client.mqttv3.*
import java.util.*


/**
 * ## JSON fields types
 * According to [jsonschema2pojo.org](https://www.jsonschema2pojo.org/)
 *
 * | Schema type | Java type            | Kotlin type              |
 * |:-----------:|:--------------------:|:------------------------:|
 * | `string`    | `java.lang.String`  | `kotlin.String`          |
 * | `number`    | `java.lang.Double`  | `kotlin.Double`          |
 * | `integer`   | `java.lang.Integer` | `kotlin.Int`             |
 * | `object`    | _generated object_   | _generated object_       |
 * | `array`     | `java.util.List`    | `kotlin.Array`           |
 * | `array`*    | `java.util.Set`     | `kotlin.collections.Set` |
 * | `null`      | `java.lang.Object`  | _anonymous objects_      |
 * | `any`       | `java.lang.Object`  | `kotlin.Any`             |
 *
 * <b>*</b> without duplicates
 *
 * <hr>
 *
 * ## Kotlin reference
 * - [`kotlin.String`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/)
 * - [`kotlin.Double`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/)
 * - [`kotlin.Int`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/)
 * - [`kotlin.Array`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/)
 * - [`kotlin.collections.Set`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-set/)
 * - [anonymous objects](https://kotlinlang.org/docs/object-declarations.html)
 *  [`kotlin.Any`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/)
 *
 *  In this sample activity you'll send a JSON made up by an Integer, a Double and a String
 */
class JsonPublisherActivity : AppCompatActivity() {
    private val tag = this.javaClass.simpleName

    private lateinit var binding: ActivityJsonPublisherBinding
    private lateinit var simpleJsonRecyclerView: SimpleJsonRecyclerView
    private lateinit var mqttClient: MqttAndroidClient
    private lateinit var recyclerView: RecyclerView
    private lateinit var preferences: SharedPreferences
    private var ip: String? = null
    private var port: String? = null
    private var username: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_subscriber)

        // bind XML layout to the class
        binding = ActivityJsonPublisherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // bind views to Activity's variables
        recyclerView = binding.payloadRecyclerView

        /* START payload configuration recycle view behaviour */
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = null

        // enable recycle view in reverse order: https://stackoverflow.com/a/63894078
        // val recyclerLayoutManager = LinearLayoutManager(this)
        // recyclerView.layoutManager = recyclerLayoutManager
        simpleJsonRecyclerView =
            SimpleJsonRecyclerView()
        recyclerView.adapter = simpleJsonRecyclerView

        simpleJsonRecyclerView.setOnAdapterItemClickListener {
            val childAdapterPosition = recyclerView.getChildAdapterPosition(it)
            val itemAtPosition = simpleJsonRecyclerView.getItemAtPosition(childAdapterPosition)
            onAdapterItemClick(itemAtPosition)
        }
        /* END payload configuration recycle view behaviour */

        // set the click listener for the button that triggers the sending of a MQTT message
        binding.publishBtn.setOnClickListener {
            val topic = binding.topicEditText.text.toString()

            // get the values from the EditText fields
            // TODO Sanitize input: prevent java.lang.NumberFormatException
            val intValue = binding.intEditText.text.toString().toInt()
            val doubleValue = binding.doubleEditText.text.toString().toDouble()
            val stringValue = binding.stringEditText.text.toString()
            val payload = JsonPayload(intValue, doubleValue, stringValue)
            Log.d(tag, payload.toString())

            // call the private function that actually publish the message on the MQTT queue
            publish(topic, payload)
        }
    }

    /**
     * Connect to MQTT broker with the username and password set in the related Settings activity
     *
     * @param username MQTT username
     * @param password MQTT password
     */
    private fun connectMQTT(username: String, password: String) {
        val options = MqttConnectOptions()
        options.mqttVersion = MqttConnectOptions.MQTT_VERSION_3_1
        options.userName = username //"mosquitto"
        options.password = password.toCharArray() // "mosquitto".toCharArray()
        options.isAutomaticReconnect = true
        options.isCleanSession = false

        try {
            mqttClient.connect(options, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Connection success: token = ${asyncActionToken.toString()}")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(
                        tag,
                        "Connection failure: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}"
                    )
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }

        mqttClient.setCallback(object : MqttCallbackExtended {
            /**
             * This method is called when a message arrives from the server.
             * Since this class only acts ad a publisher, this callback will never be called.
             */
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                Log.d(tag, "messageArrived() TOPIC: $topic | msg = $message")
            }

            /**
             * TODO Add Android Notification: [docs](https://developer.android.com/develop/ui/views/notifications)
             */
            override fun connectionLost(cause: Throwable?) {
                Log.d(tag, "connectionLost() because of ${cause.toString()}")
            }

            /**
             * TODO Add Android Toast
             */
            override fun deliveryComplete(token: IMqttDeliveryToken?) {
                Log.d(tag, "deliveryComplete() token: ${token.toString()}")
            }

            /**
             * TODO Add Android Notification: [docs](https://developer.android.com/develop/ui/views/notifications)
             */
            override fun connectComplete(reconnect: Boolean, serverURI: String?) {
                Log.d(tag, "connectComplete() reconnect = $reconnect | URI = $serverURI")
            }
        })
    }

    /**
     * Perfmon action when clicking a recycle view's element
     */
    private fun onAdapterItemClick(element: Pair<JsonPayload, Date>) {
        Log.d(tag, "onAdapterItemClick() element = ${Json.encodeToJsonElement(JsonPayload.serializer(), element.first)}")
    }

    /**
     * Publish a JSON object message on a given topic to the MQTT broker.
     *
     * @param topic name of the topic to which publish the message
     * @param payload data to be converted and sent
     * @param qos Maximum Quality of Service at which to subscribe
     */
    private fun publish(
        topic: String,
        payload: JsonPayload,
        qos: Int = 1,
        retained: Boolean = false
    ) {
        // produces JSON string using the generated serializer
        val encodedJsonPayloadString =
            Json.encodeToJsonElement(JsonPayload.serializer(), payload).toString()
        try {
            val message = MqttMessage()
            message.payload = encodedJsonPayloadString.toByteArray(Charsets.UTF_8)
            message.qos = qos
            message.isRetained = retained

            mqttClient.publish(topic, message, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "message published to $topic")

                    // create a Pair of <JsonPayload, Date> in order to avoid having to put the Date on the sent data
                    val element = Pair<JsonPayload, Date>(payload, Date())
                    simpleJsonRecyclerView.addPayloadElement(element)
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(tag, "Failed to publish message to $topic")
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    /**
     * Disconnect to MQTT broker
     */
    private fun disconnectMQTT() {
        try {
            mqttClient.disconnect(null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d(tag, "Disconnect success: token = ${asyncActionToken.toString()}")

                    // clear the list of payloads when disconnecting to the MQTT broker
                    simpleJsonRecyclerView.clearElements()
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d(
                        tag,
                        "Failed to disconnect: token = ${asyncActionToken.toString()} | exception = ${exception.toString()}"
                    )
                }
            })
        } catch (e: MqttException) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()

        disconnectMQTT()
    }

    /**
     * Settings related method
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Log.d(tag, "onCreateOptionsMenu()")

        // inflate the menu: adds items to the action bar if present
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    /**
     * Settings related method
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.action_settings) {
            startActivity(Intent(this, SettingsActivity::class.java))
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()

        // retrieve information from settings
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        ip = preferences.getString(
            EndpointSettingsActivity.IP_KEY,
            EndpointSettingsActivity.DEFAULT_IP_KEY
        )
        port = preferences.getString(
            EndpointSettingsActivity.PORT_KEY,
            EndpointSettingsActivity.DEFAULT_PORT_KEY
        )
        username = preferences.getString(
            UserSettingsActivity.USERNAME_KEY,
            UserSettingsActivity.DEFAULT_USERNAME_KEY
        )
        password = preferences.getString(
            UserSettingsActivity.PASSWORD_KEY,
            UserSettingsActivity.DEFAULT_PASSWORD_KEY
        )

        Log.d(tag, "user = $username  pwd = $password")
        // val host = "192.168.60.10" // put here your host IP address
        // val port = "1883" // default port = 1883

        // URI = [protocol]://[host-IP-address]:[host-port]
        val serverURI = "tcp://$ip:$port"
        val clientId = MqttClient.generateClientId()

        mqttClient = MqttAndroidClient(this.baseContext, serverURI, clientId, Ack.AUTO_ACK)
        connectMQTT(username!!, password!!)
        /* END MQTT client configuration */
    }
}