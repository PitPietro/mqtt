package com.pietropoluzzi.mqtt_samples.utils

import java.util.Date

/**
 * This class tells the messages' recycle view which data to show.
 * It is the easiest scenario: only show the actual message and the date of receive
 *
 * This simple line of code provides getters, setters and toString() method
 * thanks to the magic of Kotlin.
 */
class SimpleStringPayload(val payload: String, val date: Date)