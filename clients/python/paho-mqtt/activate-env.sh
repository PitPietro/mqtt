#!/bin/sh

# !!! still under develpement !!!
 
current_pwd="$PWD"
# echo $current_pwd
python_env="python-environments"
mqtt_env="mqtt-env"

if test ! -d ~/"$python_env"; then
    echo "### Creating python environments directory"
    mkdir ~/"$python_env";
fi

cd ~/"$python_env"

# create and activate the virtual environment
# sudo apt install -y virtualenv

if test ! -d ~/"$python_env"/"$mqtt_env"; then
    echo "### Creating MQTT environment"
    virtualenv "$mqtt_env";
fi

source "$mqtt-env"/bin/activate

cd "$current_pwd"
pip install -r requirements.txt

# TODO keep the terminal open