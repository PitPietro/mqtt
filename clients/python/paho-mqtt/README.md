# Python Paho MQTT

## Installation

```shell
$ cd ~/ # move to HOME directory

# create a directory for the environments
$ mkdir python-environments
$ cd python-environments/

# create and activate the virtual environment
$ sudo apt install -y virtualenv
$ virtualenv mqtt-env
$ source mqtt-env/bin/activate

# install the required packages
$ pip install -r requirements.txt

# copy the .env.txt to .env
# fill the environment variables
$ cp .env.txt .env
```

## Run the scripts

```shell
# add execution permissions to the script
$ sudo chmod +x sender-test.py

# run the script
./sender-test.py
```