#!/usr/bin/env python3

# -*- coding: utf-8 -*-
# MQTT client for python
# Docs: https://github.com/eclipse/paho.mqtt.python
# Website: pietropoluzzi.it/blog/iot/mqtt-protocol

import time
import paho.mqtt.client as mqtt
import os
from dotenv import load_dotenv
from pathlib import Path


def on_connect(mqttc, obj, flags, rc):
    print("on_connect() flags = {} - rc = {}".format(flags, rc))


def on_message(mqttc, obj, msg):
    print("on_message() obj = {} - msg = {}".format(obj, msg))


def on_publish(mqttc, obj, mid):
    print("on_publish() obj = {} - mid = {} ".format(obj, mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("on_subscribe() obj = {} - mid = {} - granted_qos = {}".format(obj, mid, granted_qos))


def on_log(mqttc, obj, level, string):
    print("Log: " + str(string))


'''
$ cd ~/ # move to HOME directory
$ mkdir python-environments
$ cd python-environments/

Create and activate the virtual environment

$ sudo apt install -y virtualenv
$ virtualenv mqtt-env
$ source mqtt-env/bin/activate
$ pip install -r requirements.txt

Rename the .env.txt to .env and fill the environment variables (only do once)
$ mv .env.txt .env
'''

if __name__ == '__main__':
    # if you want to use a specific client id, use:
    # mqtt_client = mqtt.Client("client-id")
    # the client id must be unique on the broker
    # leaving the client id parameter empty will generate a random id

    mqtt_client = mqtt.Client()
    mqtt_client.on_message = on_message
    mqtt_client.on_connect = on_connect
    mqtt_client.on_publish = on_publish
    mqtt_client.on_subscribe = on_subscribe
    mqtt_client.on_log = on_log

    # load all the variables found as environment variables
    load_dotenv()

    # load_dotenv() by default looks for the .env file in the current working directory or any parent directories
    # you can specify the path if your use case requires the .env file have to be stored elsewhere
    # dotenv_path = Path('.env')
    # load_dotenv(dotenv_path=dotenv_path)

    # take care not to override environment variables (like USER, PWD, HOME, ...)
    # load environment variables
    MQTT_USER = os.getenv('MQTT_USER')
    MQTT_PWD = os.getenv('MQTT_PWD')
    MQTT_HOST = os.getenv('MQTT_HOST')

    mqtt_client.username_pw_set(username=MQTT_USER, password=MQTT_PWD)
    mqtt_client.connect(MQTT_HOST, port=1883, keepalive=60)
    mqtt_client.loop_start()

    while True:
        infot = mqtt_client.publish("test", "HELLO", qos=0)
        infot.wait_for_publish()
        time.sleep(0.5)
